package at.spengergasse.hbgm.ui;

import at.spengergasse.hbgm.image.ILookupTable;
import at.spengergasse.hbgm.utils.IObserver;
import org.junit.Before;
import org.junit.Test;

import javax.swing.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ControlPanelTest {
    private AbstractControlPanel controlPanel;
    private List<JSlider> sliderList = new ArrayList<>();
    // some mock objects
    private ILookupTable lookupTable;
    private Viewer viewer;

    @Before
    public void setup(){
        // TODO replace with instance of your implementation
        controlPanel = new ControlPanelImplement();

        // mock objects
        lookupTable = mock(ILookupTable.class);
        viewer = mock(Viewer.class);
        when(viewer.getLookupTable()).thenReturn(lookupTable);
        when(viewer.getPatientBrowser()).thenReturn(mock(AbstractPatientBrowser.class));
        controlPanel.configure(viewer);

        // find sliders in panel
        for (int i = 0; i < controlPanel.getComponentCount(); i++){
            Component c = controlPanel.getComponent(i);
            if (c instanceof JSlider){
                JSlider sl = (JSlider) c;
                sliderList.add(sl);
                sl.setValue(sl.getMaximum());
            }
        }


    }

    @Test
    public void testLayout(){
        assertEquals(3, sliderList.size());
    }

    @Test
    public void testEvents() throws InterruptedException {
        for (JSlider sl : sliderList) {
            IObserver observer = mock(IObserver.class);
            controlPanel.registerObserver(observer);
            sl.setValue(sl.getValue()-1);
            Thread.sleep(100);
            verify(observer, atLeastOnce()).changed(controlPanel);
        }


    }
}
