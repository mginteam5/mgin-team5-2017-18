package at.spengergasse.hbgm.ui;

import at.spengergasse.hbgm.data.*;
import at.spengergasse.hbgm.utils.IObserver;
import org.junit.Before;
import org.junit.Test;

import javax.swing.*;
import javax.swing.tree.TreeNode;
import java.util.Arrays;
import java.util.Enumeration;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class PatientBrowserTest {
    private AbstractPatientBrowser browser;
    // define some mock objects
    private IPatientRepository repository;
    private Viewer viewer;
    private Image i1;
    private Image i2;
    private Image i3;
    private Image i4;
    private Image i5;
    private Image i6;
    private Image i7;
    private Image i8;
    private Series ser1;
    private Series ser2;
    private Series ser3;
    private Series ser4;
    private Study st1;
    private Study st2;
    private Study st3;
    private Patient p1;
    private Patient p2;



    @Before
    public void setUp() throws Exception {
        // TODO create instance of browser implementation
        browser = new PatientBrowserImpl();

        // prepare mock objects
        i1 = mock(Image.class);
        i2 = mock(Image.class);
        i3 = mock(Image.class);
        i4 = mock(Image.class);
        i5 = mock(Image.class);
        i6 = mock(Image.class);
        i7 = mock(Image.class);
        i8 = mock(Image.class);
        ser1 = mock(Series.class);
        ser2 = mock(Series.class);
        ser3 = mock(Series.class);
        ser4 = mock(Series.class);
        st1 = mock(Study.class);
        st2 = mock(Study.class);
        st3 = mock(Study.class);
        p1 = mock(Patient.class);
        p2 = mock(Patient.class);
        repository = mock(IPatientRepository.class);
        viewer = mock(Viewer.class);
        when(ser1.getImageList()).thenReturn(Arrays.asList(new Image[]{i1, i2}));
        when(ser2.getImageList()).thenReturn(Arrays.asList(new Image[]{i3, i4}));
        when(ser3.getImageList()).thenReturn(Arrays.asList(new Image[]{i5, i6}));
        when(ser4.getImageList()).thenReturn(Arrays.asList(new Image[]{i7, i8}));
        when(st1.getSeriesList()).thenReturn(Arrays.asList(new Series[]{ser1, ser2}));
        when(st2.getSeriesList()).thenReturn(Arrays.asList(new Series[]{ser3}));
        when(st3.getSeriesList()).thenReturn(Arrays.asList(new Series[]{ser4}));
        when(p1.getStudyList()).thenReturn(Arrays.asList(new Study[]{st1}));
        when(p2.getStudyList()).thenReturn(Arrays.asList(new Study[]{st2, st3}));
        when(repository.patientList()).thenReturn(Arrays.asList(new Patient[]{p1, p2}));
        when(viewer.getPatientRepository()).thenReturn(repository);

        JFrame f = new JFrame();
    }




    // check if configure() calls register of patient repository
    public void testConfigure() throws Exception {
        browser.configure(viewer);
        verify(repository, atLeastOnce()).registerObserver(browser);
    }

    // check if tree is built upon call of changed()
    @Test
    public void testTree() throws Exception{
        browser.configure(viewer);
        browser.changed(repository);
        TreeNode root = (TreeNode)browser.getModel().getRoot();
        // now count all the nodes for patients, studies, series and images
        int patients = 0;
        int studies = 0;
        int series = 0;
        int images = 0;
        TreeNode patNode = null;
        TreeNode stNode = null;
        TreeNode serNode = null;
        for (Enumeration<TreeNode> enP = (Enumeration<TreeNode>) root.children(); enP.hasMoreElements(); ){
            patNode = enP.nextElement();
            patients++;
            for (Enumeration<TreeNode> enSt = (Enumeration<TreeNode>) patNode.children(); enSt.hasMoreElements(); ){
                stNode = enSt.nextElement();
                studies++;
                for (Enumeration<TreeNode> enSer = (Enumeration<TreeNode>) stNode.children(); enSer.hasMoreElements(); ){
                    serNode = enSer.nextElement();
                    series++;
                    images += serNode.getChildCount();
                }
            }
        }
        assertEquals(2, patients);
        assertEquals(3, studies);
        assertEquals(4, series);
        assertEquals(8, images);
    }

    // check if observers are notified upon selection of an arbitrary node
    @Test
    public void testEvents() throws Exception{
        browser.configure(viewer);
        IObserver observer = mock(IObserver.class);
        browser.registerObserver(observer);
        browser.changed(repository);
        expandAllNodes(browser);
        browser.setSelectionRow(3);
        verify(observer, atLeastOnce()).changed(browser);
    }

    // check for an arbitrary image if the selected... methods work correctly
    @Test
    public void testSelection(){
        browser.configure(viewer);
        browser.changed(repository);
        expandAllNodes(browser);
        boolean found = false;
        for (int i = 0; i < browser.getRowCount(); i++){
            browser.setSelectionRow(i);
            Image img = browser.selectedImage();
            if (img == i8){
                found = true;
                assertSame(ser4, browser.selectedSeries());
                assertSame(st3, browser.selectedStudy());
                assertSame(p2, browser.selectedPatient());
            }
        }
        assertTrue(found);
    }


    // helper method for expanding all the nodes of the tree
    // (copied from https://stackoverflow.com/questions/15210979/how-do-i-auto-expand-a-jtree-when-setting-a-new-treemodel)
    private void expandAllNodes(JTree tree) {
        int j = tree.getRowCount();
        int i = 0;
        while(i < j) {
            tree.expandRow(i);
            i += 1;
            j = tree.getRowCount();
        }
    }
}