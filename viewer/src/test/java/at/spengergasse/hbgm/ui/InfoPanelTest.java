package at.spengergasse.hbgm.ui;

import static org.junit.Assert.*;

import at.spengergasse.hbgm.dummy.DummyInfoPanel;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;

public class InfoPanelTest {
    private AbstractInfoPanel infoPanel;
    // some mock objects
    private AbstractPatientBrowser browser;
    private Viewer viewer;

    @Before
    public void setup(){
        // TODO replace with instance of your implementation
        infoPanel = new InfoPanelImplement();
        browser = mock(AbstractPatientBrowser.class);
        when(browser.selectedPatient()).thenReturn(null);
        when(browser.selectedStudy()).thenReturn(null);
        when(browser.selectedSeries()).thenReturn(null);
        when(browser.selectedImage()).thenReturn(null);
        viewer = mock(Viewer.class);
        when(viewer.getPatientBrowser()).thenReturn(browser);
        infoPanel.configure(viewer);
    }

    @Test
    public void testEvents(){
        verify(browser, atLeastOnce()).registerObserver(infoPanel);
        infoPanel.changed(browser);
        verify(browser, atLeastOnce()).selectedPatient();
        verify(browser, atLeastOnce()).selectedStudy();
        verify(browser, atLeastOnce()).selectedSeries();
        verify(browser, atLeastOnce()).selectedImage();
    }

}
