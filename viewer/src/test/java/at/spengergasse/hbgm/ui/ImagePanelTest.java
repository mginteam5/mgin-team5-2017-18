package at.spengergasse.hbgm.ui;

import at.spengergasse.hbgm.data.Image;
import at.spengergasse.hbgm.dummy.DummyImagePanel;
import at.spengergasse.hbgm.image.ILookupTable;
import at.spengergasse.hbgm.image.IPixelMapper;
import org.junit.Before;
import org.junit.Test;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.LookupTable;
import java.io.File;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ImagePanelTest{
    private AbstractImagePanel imagePanel;

    // some mock objects
    IPixelMapper pixelMapper;
    private AbstractPatientBrowser patientBrowser;
    private ILookupTable lookupTable;
    private AbstractControlPanel controlPanel;
    private Viewer viewer;
    private BufferedImage bImage;



    @Before
    public void setup() throws Exception {
        // TODO replace with instance of your implementation
        imagePanel = new DummyImagePanel();

        // mock objects
        bImage = new BufferedImage(100,100, BufferedImage.TYPE_INT_RGB);

        Graphics g = bImage.getGraphics();
        g.setColor(Color.red);
        g.fillOval(0,0,bImage.getWidth(), bImage.getHeight());
        g.setColor(Color.blue);
        g.fillOval(0,0,bImage.getWidth()/2, bImage.getHeight()/2);
        g.setColor(Color.yellow);
        g.fillOval(0,0,bImage.getWidth()/3, bImage.getHeight()/3);

        Image image = mock(Image.class);
        patientBrowser = mock(AbstractPatientBrowser.class);
        when(patientBrowser.selectedImage()).thenReturn(image);
        pixelMapper = mock(IPixelMapper.class);
        when(pixelMapper.mapImage(image)).thenReturn(bImage);

        viewer = mock(Viewer.class);
        lookupTable = mock(ILookupTable.class);
        controlPanel = mock(AbstractControlPanel.class);
        when(viewer.getPatientBrowser()).thenReturn(patientBrowser);
        when(viewer.getPixelMapper()).thenReturn(pixelMapper);
        when(viewer.getLookupTable()).thenReturn(lookupTable);
        when(viewer.getControlPanel()).thenReturn(controlPanel);

        imagePanel.configure(viewer);

    }

    @Test
    public void testConfigure(){
        verify(patientBrowser, atLeastOnce()).registerObserver(imagePanel);
        verify(lookupTable, atLeastOnce()).registerObserver(imagePanel);
        verify(controlPanel, atLeastOnce()).registerObserver(imagePanel);
    }

    @Test
    public void testPaint() throws Exception{

        JFrame f = new JFrame();
        f.setSize(500,500);
        f.add(imagePanel);
        f.setVisible(true);
        imagePanel.changed(patientBrowser);
        Thread.sleep(1000);

        Robot robot = new Robot();
        BufferedImage img = robot.createScreenCapture(new Rectangle(500,500));
        Point p = imagePanel.getLocationOnScreen();
        BufferedImage img2 = img.getSubimage(p.x, p.y, bImage.getWidth(), bImage.getHeight());

        int diff = 0;
        for (int r = 0; r < bImage.getHeight(); r++){
            for (int c = 0; c < bImage.getWidth(); c++) {
                diff += bImage.getRGB(c, r) - img2.getRGB(c, r);
            }
        }

        assertEquals(0, diff);

    }



}
