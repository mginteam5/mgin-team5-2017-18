package at.spengergasse.hbgm.dummy;

import at.spengergasse.hbgm.data.Image;
import at.spengergasse.hbgm.image.IPixelMapper;
import at.spengergasse.hbgm.ui.Viewer;
import at.spengergasse.hbgm.utils.IBaseComponent;
import at.spengergasse.hbgm.utils.IObserver;

import java.awt.image.BufferedImage;

/**
 * dummy implementation
 */

public class DummyPixelMapper implements IPixelMapper {
    public BufferedImage mapImage(Image img) throws Exception {
        return null;
    }

    public void registerObserver(IObserver observer) {

    }

    public void removeObserver(IObserver observer) {

    }

    public void configure(Viewer viewer) {

    }


    public void changed(IBaseComponent c) {

    }
}
