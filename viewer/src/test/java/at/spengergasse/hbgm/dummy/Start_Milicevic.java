package at.spengergasse.hbgm.dummy;

import at.spengergasse.hbgm.data.PatientRepositoryImpl;
import at.spengergasse.hbgm.ui.*;

import javax.swing.*;

public class Start_Milicevic {
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Viewer viewer = new Viewer();

        viewer.setPatientRepository(new PatientRepositoryImpl());
        viewer.setPixelMapper(new DummyPixelMapper());
        viewer.setLookupTable(new LookupTableImplement());
        viewer.setControlPanel(new ControlPanelImplement());
        viewer.setImagePanel(new DummyImagePanel());
        viewer.setInfoPanel(new InfoPanelImplement());
        viewer.setPatientBrowser(new PatientBrowserImpl());

        viewer.configure();

        viewer.setSize(800,600);
        viewer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        viewer.setVisible(true);
    }
}
