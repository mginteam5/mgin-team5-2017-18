package at.spengergasse.hbgm.dummy;

import at.spengergasse.hbgm.data.IPatientRepository;
import at.spengergasse.hbgm.data.Patient;
import at.spengergasse.hbgm.ui.Viewer;
import at.spengergasse.hbgm.utils.IBaseComponent;
import at.spengergasse.hbgm.utils.IObserver;

import java.io.File;
import java.util.List;

/**
 * dummy implementation
 */

public class DummyPatientRepository implements IPatientRepository {
    public void addDicomFile(File dicomFile) throws Exception {

    }

    public void addDicomFolder(File folder) throws Exception {

    }

    public List<Patient> patientList() {
        return null;
    }

    public void registerObserver(IObserver observer) {

    }

    public void removeObserver(IObserver observer) {

    }

    public void configure(Viewer viewer) {

    }


    public void changed(IBaseComponent c) {

    }
}
