package at.spengergasse.hbgm.dummy;

import at.spengergasse.hbgm.data.Image;
import at.spengergasse.hbgm.data.Patient;
import at.spengergasse.hbgm.data.Series;
import at.spengergasse.hbgm.data.Study;
import at.spengergasse.hbgm.ui.AbstractPatientBrowser;
import at.spengergasse.hbgm.ui.Viewer;
import at.spengergasse.hbgm.utils.IBaseComponent;
import at.spengergasse.hbgm.utils.IObserver;

import javax.swing.border.TitledBorder;

/**
 * dummy implementation
 */

public class DummyPatientBrowser extends AbstractPatientBrowser {

    public DummyPatientBrowser()
    {
        setBorder(new TitledBorder("Patient Browser"));
    }

    public Patient selectedPatient() {
        return null;
    }

    public Study selectedStudy() {
        return null;
    }

    public Series selectedSeries() {
        return null;
    }

    public Image selectedImage() {
        return null;
    }

    public void registerObserver(IObserver observer) {

    }

    public void removeObserver(IObserver observer) {

    }

    public void configure(Viewer viewer) {

    }


    public void changed(IBaseComponent c) {

    }
}
