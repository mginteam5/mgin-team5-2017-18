package at.spengergasse.hbgm.dummy;

import at.spengergasse.hbgm.image.ILookupTable;
import at.spengergasse.hbgm.ui.Viewer;
import at.spengergasse.hbgm.utils.IBaseComponent;
import at.spengergasse.hbgm.utils.IObserver;

/**
 * dummy implementation
 */

public class DummyLookupTable implements ILookupTable {
    public int lookup(int value) {
        return 0;
    }

    public void setCenter(int center) throws Exception {

    }

    public int getCenter() {
        return 0;
    }

    public void setWidth(int width) throws Exception {

    }

    public int getWidth() {
        return 0;
    }

    public void setAlpha(int alpha) throws Exception {

    }

    public int getAlpha() {
        return 0;
    }

    public void registerObserver(IObserver observer) {

    }

    public void removeObserver(IObserver observer) {

    }

    public void configure(Viewer viewer) {

    }


    public void changed(IBaseComponent c) {

    }
}
