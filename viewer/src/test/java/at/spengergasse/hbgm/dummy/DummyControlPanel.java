package at.spengergasse.hbgm.dummy;

import at.spengergasse.hbgm.ui.AbstractControlPanel;
import at.spengergasse.hbgm.ui.Viewer;
import at.spengergasse.hbgm.utils.IBaseComponent;
import at.spengergasse.hbgm.utils.IObserver;

import javax.swing.border.TitledBorder;

/**
 * dummy implementation
 */

public abstract class DummyControlPanel extends AbstractControlPanel {

    public DummyControlPanel()
    {
        setBorder(new TitledBorder("Control Panel"));
    }

    public void registerObserver(IObserver observer) {

    }

    public void removeObserver(IObserver observer) {

    }

    public void configure(Viewer viewer) {

    }


    public void changed(IBaseComponent c) {

    }
}
