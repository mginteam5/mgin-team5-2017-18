package at.spengergasse.hbgm;

import at.spengergasse.hbgm.dummy.*;
import at.spengergasse.hbgm.ui.ControlPanelImplement;
import at.spengergasse.hbgm.ui.InfoPanelImplement;
import at.spengergasse.hbgm.ui.LookupTableImplement;
import at.spengergasse.hbgm.ui.Viewer;

import javax.swing.*;

public class Start_Martic {
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Viewer viewer = new Viewer();

        viewer.setPatientRepository(new DummyPatientRepository());
        viewer.setPixelMapper(new DummyPixelMapper());
        viewer.setLookupTable(new LookupTableImplement());
        viewer.setControlPanel(new ControlPanelImplement());
        viewer.setImagePanel(new DummyImagePanel());
        viewer.setInfoPanel(new InfoPanelImplement());
        viewer.setPatientBrowser(new DummyPatientBrowser());

        viewer.configure();

        viewer.setSize(800,600);
        viewer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        viewer.setVisible(true);
    }
}
