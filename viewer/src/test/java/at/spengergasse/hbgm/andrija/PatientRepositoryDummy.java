package at.spengergasse.hbgm.andrija;

import at.spengergasse.hbgm.data.*;
import at.spengergasse.hbgm.ui.Viewer;
import at.spengergasse.hbgm.utils.IBaseComponent;
import at.spengergasse.hbgm.utils.IObserver;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PatientRepositoryDummy implements IPatientRepository{


    @Override
    public void addDicomFolder(File folder) throws Exception {

    }

    @Override
    public List<Patient> patientList() {
        ArrayList<Patient> liste = new ArrayList<Patient>();
        Patient p = new Patient(null);
        Study st = new Study(null);
        Series sr = new Series(null);
        Image im = new Image(null, null);
        liste.add(p);
        p.getStudyList().add(st);
        st.getSeriesList().add(sr);
        sr.getImageList().add(im);




        return liste;
    }

    @Override
    public void registerObserver(IObserver observer) {

    }

    @Override
    public void removeObserver(IObserver observer) {

    }

    @Override
    public void configure(Viewer viewer) {

    }

    @Override
    public void changed(IBaseComponent c) {

    }
}
