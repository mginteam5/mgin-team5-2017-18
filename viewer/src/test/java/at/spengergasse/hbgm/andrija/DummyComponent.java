package at.spengergasse.hbgm.andrija;

import at.spengergasse.hbgm.ui.Viewer;
import at.spengergasse.hbgm.utils.IBaseComponent;
import at.spengergasse.hbgm.utils.IObserver;

public class DummyComponent implements IBaseComponent {
    @Override
    public void registerObserver(IObserver observer) {

    }

    @Override
    public void removeObserver(IObserver observer) {

    }

    @Override
    public void configure(Viewer viewer) {

    }

    @Override
    public void changed(IBaseComponent c) {
        System.out.println("Benachrichtigung von Komponente " + c);
    }
}
