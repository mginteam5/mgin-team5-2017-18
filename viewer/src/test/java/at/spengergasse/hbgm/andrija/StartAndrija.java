package at.spengergasse.hbgm.andrija;

import at.spengergasse.hbgm.data.IPatientRepository;
import at.spengergasse.hbgm.dummy.*;
import at.spengergasse.hbgm.ui.AbstractPatientBrowser;
import at.spengergasse.hbgm.ui.ControlPanelImplement;
import at.spengergasse.hbgm.ui.PatientBrowserImpl;
import at.spengergasse.hbgm.ui.Viewer;

import javax.swing.*;

public class StartAndrija {
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Viewer viewer = new Viewer();

        IPatientRepository repo = new PatientRepositoryDummy();
        viewer.setPatientRepository(repo);
        viewer.setPixelMapper(new DummyPixelMapper());
        viewer.setLookupTable(new DummyLookupTable());
        viewer.setControlPanel(new ControlPanelImplement());
        viewer.setImagePanel(new DummyImagePanel());
        viewer.setInfoPanel(new DummyInfoPanel());
        AbstractPatientBrowser browser = new PatientBrowserImpl();
        viewer.setPatientBrowser(browser);

        viewer.configure();

        browser.registerObserver(new DummyComponent());

        browser.changed(repo);

        viewer.setSize(800,600);
        viewer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        viewer.setVisible(true);
    }
}
