package at.spengergasse.hbgm;

import at.spengergasse.hbgm.dummy.*;
import at.spengergasse.hbgm.ui.ControlPanelImplement;
import at.spengergasse.hbgm.ui.Viewer;

import javax.swing.*;

public class Start {
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Viewer viewer = new Viewer();

        viewer.setPatientRepository(new DummyPatientRepository());
        viewer.setPixelMapper(new DummyPixelMapper());
        viewer.setLookupTable(new DummyLookupTable());
        viewer.setControlPanel(new ControlPanelImplement());
        viewer.setImagePanel(new DummyImagePanel());
        viewer.setInfoPanel(new DummyInfoPanel());
        viewer.setPatientBrowser(new DummyPatientBrowser());

        viewer.configure();

        viewer.setSize(800,600);
        viewer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        viewer.setVisible(true);
    }
}
