package at.spengergasse.hbgm.image;

import at.spengergasse.hbgm.ui.LookupTableImplement;
import at.spengergasse.hbgm.utils.IObserver;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;



public class LookupTableTest {
    private ILookupTable lookupTable;

    @Before
    public void setup() throws Exception{
        // TODO replace with instance of your implementation
        lookupTable = new LookupTableImplement();
        lookupTable.setAlpha(255);
        lookupTable.setCenter(100);
        lookupTable.setWidth(100);

    }

    // check that alpha is correctly calculated
    @Test
    public void testAlpha() throws Exception {
        lookupTable.setAlpha(0);
        assertEquals(0, lookupTable.lookup(100) & 0xFF000000);
        lookupTable.setAlpha(100);
        assertEquals(100 << 24, lookupTable.lookup(100) & 0xFF000000);
        lookupTable.setAlpha(200);
        assertEquals(200 << 24, lookupTable.lookup(100) & 0xFF000000);
    }

    // check that red, green, blue are correctly calculated
    @Test
    public void testRGB() throws Exception {
        // center and width = 100
        // check different values (blue component)
        assertEquals(255, lookupTable.lookup(150) & 0x000000FF);
        assertEquals(0, lookupTable.lookup(50) & 0x000000FF);
        assertEquals(127, lookupTable.lookup(100) & 0x000000FF);
        assertEquals(255, lookupTable.lookup(1000) & 0x000000FF);
        assertEquals(0, lookupTable.lookup(20) & 0x000000FF);

        // check red and green
        assertEquals(127 << 8, lookupTable.lookup(100) & 0x0000FF00);
        assertEquals(127 << 16, lookupTable.lookup(100) & 0x00FF0000);


    }


    // width must be greater than 0 - check for exception
    @Test
    public void testInvalidWidth(){
        try{
            lookupTable.setWidth(0);
            fail("width must not be 0 --> exception expected");
        } catch(Exception ex){
            // intentionally left blank
            // exception is expected here
        }

        try{
            lookupTable.setWidth(-1);
            fail("width must not be less than 0 --> exception expected");
        } catch(Exception ex){
            // intentionally left blank
            // exception is expected here
        }
    }

    // observers should be called if values are changed
    @Test
    public void testEvents() throws Exception {
        IObserver observer = mock(IObserver.class);
        lookupTable.registerObserver(observer);
        lookupTable.setAlpha(10);
        verify(observer, atLeastOnce()).changed(lookupTable);

        observer = mock(IObserver.class);
        lookupTable.registerObserver(observer);
        lookupTable.setCenter(10);
        verify(observer, atLeastOnce()).changed(lookupTable);

        observer = mock(IObserver.class);
        lookupTable.registerObserver(observer);
        lookupTable.setWidth(10);
        verify(observer, atLeastOnce()).changed(lookupTable);

    }

}
