package at.spengergasse.hbgm.image;

import at.spengergasse.hbgm.data.Image;
import at.spengergasse.hbgm.dummy.DummyPixelMapper;
import at.spengergasse.hbgm.ui.Viewer;
import org.junit.Before;
import org.junit.Test;

import java.awt.image.BufferedImage;
import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class PixelMapperTest {
    private IPixelMapper pixelMapper;
    // some mock objects
    private ILookupTable lookupTable;
    private Viewer viewer;
    private Image image;


    @Before
    public void setup() throws IOException {
        // TODO replace with instance of your implementation
        pixelMapper = new DummyPixelMapper();

        // mock objects
        image = mock(Image.class);
        when(image.getRows()).thenReturn(5);
        when(image.getColumns()).thenReturn(10);
        short[] pixels = new short[50];
        for (int i = 0; i < 50; i++)
            pixels[i] = (short)i;
        when(image.getPixelValues()).thenReturn(pixels);

        lookupTable = mock(ILookupTable.class);
        for (int i = 1; i <= 50; i++)
            when(lookupTable.lookup(i)).thenReturn(i*2);

        viewer = mock(Viewer.class);
        when(viewer.getLookupTable()).thenReturn(lookupTable);

        pixelMapper.configure(viewer);
    }

    // check image mapping
    @Test
    public void testMap() throws  Exception{
        BufferedImage bi = pixelMapper.mapImage(image);
        assertEquals(10, bi.getWidth());
        assertEquals(5, bi.getHeight());
        for (int r = 0; r < bi.getHeight(); r++){
            for (int c = 0; c < bi.getWidth(); c++){
                int value = r * bi.getWidth() + c;
                assertEquals(2 * value, bi.getRGB(c,r));
            }
        }
    }

}