package at.spengergasse.hbgm.data;

import at.spengergasse.hbgm.utils.IObserver;
import org.junit.Before;
import org.junit.Test;


import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

// this test class relies on the images in images directory
public class PatientRepositoryTest {

    private IPatientRepository repository;

    @Before
    public void setup()
    {
        //TODO: create instance of your own patient repository
        setRepository(new PatientRepositoryImpl());
    }



    // call addDicomFolder(), load a single dicom file and check
    // some of the properties
    @Test
    public void addDicomFolder() throws Exception {
        getRepository().addDicomFolder(new File("src/test/images/test1"));
        List<Patient> patList = getRepository().patientList();
        assertEquals(1, patList.size());
        List<Study> studyList = patList.get(0).getStudyList();
        assertEquals(1, studyList.size());
        List<Series> serList = studyList.get(0).getSeriesList();
        assertEquals(1, serList.size());
        List<Image> imgList = serList.get(0).getImageList();
        assertEquals(1, imgList.size());
        Patient p = patList.get(0);
        Study st = studyList.get(0);
        Series se = serList.get(0);
        Image i = imgList.get(0);
        assertEquals("BRAINIX", p.getName().trim());

        assertEquals(new SimpleDateFormat("dd.MM.yyyy").parse("1.12.2006"), st.getStudyDate());
        assertEquals(512, i.getRows());
        assertEquals(262144, i.getPixelValues().length);
    }

    // call addDicomFolder() loading several dicom files
    // (different patients, studies, series, images)
    // and check correct loading
    @Test
    public void addDicomfolder2() throws Exception {
        for (int i = 0; i < 2; i++) {
            getRepository().addDicomFolder(new File("src/test/images/test2"));
            PatientListTester tester = new PatientListTester(getRepository().patientList());
            assertEquals(2, tester.getPatientCount());
            assertEquals(2, tester.getStudyCount());
            assertEquals(3, tester.getSeriesCount());
            assertEquals(10, tester.getImageCount());
        }
    }

    // check if observer is called upon loading a DICOM folder
    @Test
    public void registerObserver() throws Exception {
        IObserver observer = mock(IObserver.class);
        getRepository().registerObserver(observer);
        getRepository().addDicomFolder(new File("src/test/images"));
        verify(observer, atLeastOnce()).changed(getRepository());
    }


    public IPatientRepository getRepository() {
        return repository;
    }

    public void setRepository(IPatientRepository repository) {
        this.repository = repository;
    }
}