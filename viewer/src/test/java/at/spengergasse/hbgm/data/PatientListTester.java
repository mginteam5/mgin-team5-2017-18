package at.spengergasse.hbgm.data;

import java.util.List;

// helper class for counting image, series, studies, patients
// objects in a list of patients
public class PatientListTester {
    private int patientCount;
    private int studyCount;
    private int seriesCount;
    private int imageCount;

    public PatientListTester(List<Patient> list){
        for (Patient p : list){
            patientCount++;
            for (Study st : p.getStudyList()){
                studyCount++;
                for (Series ser : st.getSeriesList()){
                    seriesCount++;
                    for (Image i : ser.getImageList()){
                        imageCount++;
                    }
                }
            }
        }
    }

    @Override
    public String toString() {
        return "PatientListTester{" +
                "patientCount=" + patientCount +
                ", studyCount=" + studyCount +
                ", seriesCount=" + seriesCount +
                ", imageCount=" + imageCount +
                '}';
    }

    public int getPatientCount() {
        return patientCount;
    }

    public int getStudyCount() {
        return studyCount;
    }

    public int getSeriesCount() {
        return seriesCount;
    }

    public int getImageCount() {
        return imageCount;
    }
}
