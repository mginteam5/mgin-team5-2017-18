package at.spengergasse.hbgm.data;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.DicomInputStream;

import java.io.File;
import java.io.IOException;

/**
 * data of an image instance
 */


public class Image {
    private String sopInstanceUID;
    private File dicomFile;
    private int rows;
    private int columns;
    private short minValue;
    private short maxValue;

    public Image(DicomObject dcm, File dicomFile) {
        this.dicomFile = dicomFile;
        // TODO all the attribute values must be determined from the dicom file
        sopInstanceUID = dcm.getString(Tag.SOPInstanceUID);
        rows = dcm.getInt(Tag.Rows);
        columns = dcm.getInt(Tag.Columns);

    }

    public String getSopInstanceUID() {
        return sopInstanceUID;
    }

    public File getDicomFile() {
        return dicomFile;
    }

    public short[] getPixelValues() throws IOException {
        // TODO: implement
        /*
        for saving RAM space pixel values are read from dicom file only if needed
         */

        DicomInputStream dis = new DicomInputStream(dicomFile);
        DicomObject dd = dis.readDicomObject();
        short[] pv = dd.getShorts(Tag.PixelData);
        dis.close();
        return pv;
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public short getMinValue() {
        return minValue;
    }

    public short getMaxValue() {
        return maxValue;
    }
}
