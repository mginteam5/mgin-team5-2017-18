package at.spengergasse.hbgm.data;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * contains informations of series and a list of images
 */

public class Series {
    private String seriesInstanceUID;
    private Date acquisitionTime;

    private List<Image> imageList = new ArrayList<Image>();


    public Series(DicomObject dcm) {
        // TODO get attribute values from dicom object
        seriesInstanceUID = dcm.getString(Tag.SeriesInstanceUID);
        acquisitionTime = dcm.getDate(Tag.AcquisitionTime);
    }

    public Image add(Image img) {
        // TODO implement:
        /*
        In the parameter an image instance is passed to this method.
        Check if the image list already contains an image with equal
        sopInstanceUID. If yes - return a reference to the already
        contained instance. If no - insert parameter instance and return
        a reference to it.
         */
        for (Image i : imageList) {
            if (i.getSopInstanceUID().equals(img.getSopInstanceUID())) {
                return i;
            }
        }
        imageList.add(img);
        return img;
    }
    public String getSeriesInstanceUID() {
        return seriesInstanceUID;
    }

    public Date getAcquisitionTime() {
        return acquisitionTime;
    }

    public List<Image> getImageList() {
        return imageList;
    }
}
