package at.spengergasse.hbgm.data;

import at.spengergasse.hbgm.ui.Viewer;
import at.spengergasse.hbgm.utils.IBaseComponent;
import at.spengergasse.hbgm.utils.IObserver;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.DataFormatException;

import at.spengergasse.hbgm.utils.ObserverManager;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.data.DicomObject;

import javax.xml.crypto.Data;

public class  PatientRepositoryImpl implements IPatientRepository
{

    private List<Patient> plist = new ArrayList<Patient>();
   // private ArrayList<IObserver> observers = new ArrayList<IObserver>();
    private ObserverManager observerManager = new ObserverManager();
    private Viewer viewer;

    @Override
    public void addDicomFolder(File folder) throws Exception {
        if (!folder.isDirectory())
        {
            throw new DataFormatException("Das ist kein Ordner");
        }
        else {

            for (File f : folder.listFiles()) //wenn file mit .dcm endet dann loaden
            {
                if (f.isFile() && f.getName().toLowerCase().endsWith(".dcm")) {
                    loadFile(f);

                }
            }
        }
        observerManager.notifyObservers(this);

    }


    public void loadFile (File f )throws Exception {
        DicomInputStream input = new DicomInputStream(f);
        DicomObject dcm = input.readDicomObject();
        Patient p = add(new Patient(dcm));
        Study study = p.add(new Study(dcm));
        Series serie = study.add(new Series(dcm));
        serie.add(new Image(dcm, f));


        //sich selbst übergeben -> Observer benachrichtigen

    }

    private Patient add(Patient patient)
    {
        Patient p1 = null;
        for (Patient p : plist)
        {
            if(p.getPatientID().equals(patient.getPatientID()))
            {
                p1 = p;
                break;
            }
        }
        if(p1 == null)
        {
            plist.add(patient);
            p1 = patient;
        }
        return p1;
    }



    @Override
    public List<Patient> patientList() {
        return plist;
    }

    @Override
    public void registerObserver(IObserver observer) {

        observerManager.registerObserver(observer);
    }

    @Override
    public void removeObserver(IObserver observer) {
        observerManager.registerObserver(observer);
    }

    @Override
    public void configure(Viewer viewer) {
        this.viewer = viewer;
    }

    @Override
    public void changed(IBaseComponent c) {

    }
}


