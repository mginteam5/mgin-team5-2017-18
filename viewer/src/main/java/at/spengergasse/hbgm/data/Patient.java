package at.spengergasse.hbgm.data;


import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * contains patient data and the patient's studies list
 */

public class Patient {
    private String patientID;
    private String name;
    private Date birthDate;

    private List<Study> studyList = new ArrayList<Study>();

    public Patient(DicomObject dcm) {
        // TODO obtain attribute values from dicom object
        patientID = dcm.getString(Tag.PatientID);
        name = dcm.getString(Tag.PatientName);
        birthDate = dcm.getDate(Tag.PatientBirthDate);

    }

    public Study add(Study st){
        // TODO implement:
        /*
        In the parameter a study instance is passed to this method.
        Check if the studies list already contains a study with equal
        studyInstanceUID. If yes - return a reference to the already
        contained instance. If no - insert parameter instance and return
        a reference to it.
         */
        for(Study s : studyList) {
            if(s.getStudyInstanceUID().equals(st.getStudyInstanceUID())){
                return s;
            }
        }
        studyList.add(st);
        return st;
    }

    public String getPatientID() { return patientID; }

    public String getName() {
        return name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public List<Study> getStudyList() {
        return studyList;
    }
}
