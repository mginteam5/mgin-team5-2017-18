package at.spengergasse.hbgm.data;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * contains data of the study and a list of series
 */

public class Study {
    private String studyInstanceUID;
    private Date studyDate;

    private List<Series> seriesList = new ArrayList<Series>();

    public Study(DicomObject dcm) {
        // TODO get all the attribute values from the dicom object
        studyInstanceUID = dcm.getString(Tag.StudyInstanceUID);
        studyDate = dcm.getDate(Tag.StudyDate);
    }

    public Series add(Series ser) {
        // TODO implement:
        /*
        In the parameter a series instance is passed to this method.
        Check if the series list already contains a series with equal
        seriesInstanceUID. If yes - return a reference to the already
        contained instance. If no - insert parameter instance and return
        a reference to it.
         */
        for (Series s : seriesList) {
            if (s.getSeriesInstanceUID().equals(ser.getSeriesInstanceUID())) {
                return s;
            }
        }
        seriesList.add(ser);
        return ser;
    }

    public String getStudyInstanceUID() {
        return studyInstanceUID;
    }

    public Date getStudyDate() {
        return studyDate;
    }

    public List<Series> getSeriesList() {
        return seriesList;
    }
}
