package at.spengergasse.hbgm;

import at.spengergasse.hbgm.data.PatientRepositoryImpl;
import at.spengergasse.hbgm.ui.*;

import javax.swing.*;

public class Start {
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

            Viewer viewer = new Viewer();

            viewer.setPatientRepository(new PatientRepositoryImpl());
            viewer.setPixelMapper(new RealPixelMapper());
            viewer.setLookupTable(new LookupTableImplement());
            viewer.setControlPanel(new ControlPanelImplement());
            viewer.setImagePanel(new ImagePanelImplement());
            viewer.setInfoPanel(new InfoPanelImplement());
            viewer.setPatientBrowser(new PatientBrowserImpl());

            viewer.configure();

            viewer.setSize(800,600);
            viewer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            viewer.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

    }
}
