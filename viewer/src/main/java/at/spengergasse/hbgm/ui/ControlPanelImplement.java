package at.spengergasse.hbgm.ui;

import at.spengergasse.hbgm.utils.IBaseComponent;
import at.spengergasse.hbgm.utils.IObserver;
//import com.sun.java.util.jar.pack.Attribute;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
//import javax.swing.event.ChangeEvent;
//import javax.swing.event.ChangeListener;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created by Clarissa Martic on 24.11.2017.
 * changed 28.11.2017--> Layout geändert
 */
public class ControlPanelImplement extends AbstractControlPanel {
    private Viewer viewer;

    protected JSlider slWidth = new JSlider(JSlider.HORIZONTAL, -10, 10,0); //Width --> Startwert/Kontrast
    protected JSlider slCenter = new JSlider(JSlider.HORIZONTAL,0,100,100); // Center
    protected JSlider slAlpha = new JSlider(JSlider.HORIZONTAL, 0,100,100); //Alpha wert --> Transparenz/Stärke des Bildes



    private ArrayList<IObserver> observerList = new ArrayList<IObserver>();
    public ControlPanelImplement ()
    {
        this.setLayout(new GridLayout(0,1));
        this.add(new JLabel("Kontrast: "));
        this.add(slWidth);
        this.add(new JLabel("Center: "));
        this.add(slCenter);
        this.add(new JLabel("Transparenz: "));
        this.add(slAlpha);



        slAlpha.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                changeAlpha();
            }
        });
        slCenter.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                changeCenter();
            }
        });
        slWidth.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                changeWidth();
            }
        });
    }

    public void changeCenter() {
        try
        {
            viewer.getLookupTable().setCenter(slCenter.getValue());
            System.out.println("center: " + slCenter.getValue());

            for (IObserver Observer : observerList)
            {
                Observer.changed(ControlPanelImplement.this);
            }
        } catch (Exception e1)
        {
            e1.printStackTrace();
        }
    }
    public void changeAlpha() {
        try
        {
            viewer.getLookupTable().setCenter(slAlpha.getValue());
            System.out.println("alpha: " + slCenter.getValue());

            for (IObserver Observer : observerList)
            {
                Observer.changed(ControlPanelImplement.this);
            }
        } catch (Exception e1)
        {
            e1.printStackTrace();
        }
    }
    public void changeWidth() {
        try
        {
            viewer.getLookupTable().setCenter(slWidth.getValue());
            System.out.println("width: " + slWidth.getValue());

            for (IObserver Observer : observerList)
            {
                Observer.changed(ControlPanelImplement.this);
            }
        } catch (Exception e1)
        {
            e1.printStackTrace();
        }
    }



    public void registerObserver(IObserver observer) {
        this.observerList.add(observer); // Observer der Observerlist hinzufügen
    }

    public void removeObserver(IObserver observer) {
        for(int zaehler  = 0; zaehler< observerList.size(); zaehler++){
            if(observer == observerList.get(zaehler)){
                removeObserver(observer); // Observer löschen --> eigene Funktion
            }
        }
    }

    public void configure(Viewer viewer) {
        this.viewer = viewer; //Zugriff auf den Viewer erstellen

    }

    public void changed(IBaseComponent c) {
        NotifyObservers();
        changeAlpha();
        changeCenter();
        changeWidth();
    }

    private void NotifyObservers (){
        for(IObserver o : observerList) //Benachrichtigung an alle Observer --> Änderung findet in DIESER Komponente statt
        {
            o.changed(this); // this --> diese Komponente

        }

    }

    @Override
    public void setHighest(int highest) {

    }


    /**
     private static void createAndShowGUI() {
     //Create and set up the window.
     GridLayoutDemo frame = new GridLayoutDemo("GridLayoutDemo");
     frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     //Set up the content pane.
     frame.addComponentsToPane(frame.getContentPane());
     //Display the window.
     frame.pack();
     frame.setVisible(true);
     }**/
}

