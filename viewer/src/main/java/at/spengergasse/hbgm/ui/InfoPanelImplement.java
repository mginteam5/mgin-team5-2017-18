package at.spengergasse.hbgm.ui;

import at.spengergasse.hbgm.data.*;
import at.spengergasse.hbgm.data.Image;
import at.spengergasse.hbgm.utils.IBaseComponent;
import at.spengergasse.hbgm.utils.IObserver;


import javax.swing.*;
import java.awt.*;

import java.util.*;
import java.util.List;

/**
 * Created by Clarissa Martic on 24.11.2017.
 * changed 28.11.2017
 *
 *
 *
 * IDEEENGANG:
 * Der Aufbauwird der selbe wie beim Control Panel --> Text Fields & JLabels
 * Wird angezeigt --> Übernahme der Bilddaten muss implementiert werden
 *
 *
 * Letzte Änderung: 16.01.2018
 * Observer registrieren
 * Patient wird abgefragt bevor auf die ID zugegriffen wird
 */
//Erster Entwurf InfoPanel --> Übernahme der Daten des PAtientbrowsers fehlt noch

public class InfoPanelImplement extends AbstractInfoPanel {

    private Viewer viewer = new Viewer(); //configure
    private static List<IObserver> observerList = new ArrayList<IObserver>();
    private JLabel tf1= new JLabel();// TextField für die Informationen des ausgewählten Punktes
    private JLabel tf2= new JLabel();
    private JLabel tf3= new JLabel();
    private JLabel d1= new JLabel();// TextField für die Informationen des ausgewählten Punktes
    private JLabel d2= new JLabel();
    private JLabel d3= new JLabel();

    /***
     * Statt TextFields Labels
     */


    public InfoPanelImplement(){
        this.setLayout(new GridLayout(0,1));
        d1.setText("InfoPanel");
        d2.setText("Data shows when selected");
        this.add(d1);
        this.add(tf1);
        this.add(d2);
        this.add(tf2);
        this.add(d3);
        this.add(tf3);
    }
    @Override
    public void registerObserver(IObserver observer) {
        observerList.add(observer);
    }

    @Override
    public void removeObserver(IObserver observer) {
        for(int zaehler  = 0; zaehler< observerList.size(); zaehler++)
        {
            if(observer == observerList.get(zaehler))
            {
                removeObserver(observer); // Observer löschen --> eigene Funktion
            }
        }
    }

    @Override
    public void configure(Viewer viewer) {
        this.viewer = viewer;
        // this.viewer.setLayout(new GridLayout(0,1));
        viewer.getPatientBrowser().registerObserver(this);//Observer Registrieren //observer: this --> automatisch generiert

    }

    @Override
    public void changed(IBaseComponent c) {
        //basically setTextMethode
        AbstractPatientBrowser browser = this.viewer.getPatientBrowser(); //Patient BrowserImolement einfügen!!!
        Patient p = browser.selectedPatient();
        Study st = browser.selectedStudy();
        Series s = browser.selectedSeries();
        Image im = browser.selectedImage();

        try{
            if (p != null){
                d2.setText("Name");
                d3.setText("Birthdate");
                d1.setText("Patient ID");
                tf1.setText(p.getPatientID());
                tf2.setText(p.getName());
                tf3.setText(p.getBirthDate().toString());
                NotifyObservers();

            }
            else if(st!= null){ //wenn Studie ausgewählt
                // tf2.setText(st.getStudyInstanceUID); //Beschreibung der Studie
                //Resizable?? setzen?
                d2.setText("Study UID");
                d3.setText("Date of the Study");
                d1.setText("Patient ID");
                tf1.setText(p.getPatientID());
                tf2.setText(st.getStudyInstanceUID());
                tf3.setText(st.getStudyDate().toString());

                NotifyObservers();
            }
            else if(s != null){

                //Wenn Serie Ausgewählt ist
                d2.setText("Series UID");
                d3.setText("Aquisition Time");
                d1.setText("Patient ID");
                tf1.setText(p.getPatientID());
                tf2.setText(s.getSeriesInstanceUID());
                tf3.setText(s.getAcquisitionTime().toString());
                NotifyObservers();
            }
            else if (im!=null){

                //Wenn Bild ausgewählt
                d2.setText("Image UID");
                d3.setText("Dicom File");
                d1.setText("Patient ID");
                tf1.setText(p.getPatientID());
                tf2.setText(im.getSopInstanceUID());
                tf3.setText(im.getDicomFile().toString());
                NotifyObservers();
            }}
        catch (Exception e){

            tf1.setText("Nothing selected");
            tf2.setText("Please select a field");
            //Exeption
        }

    }
    private void NotifyObservers (){
        for(IObserver o : observerList) //Benachrichtigung an alle Observer --> Änderung findet in DIESER Komponente statt
        {
            o.changed(this); // this --> diese Komponente

        }

    }



}
