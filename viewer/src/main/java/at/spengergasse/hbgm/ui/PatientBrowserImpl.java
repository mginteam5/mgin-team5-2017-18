package at.spengergasse.hbgm.ui;
import at.spengergasse.hbgm.data.Image;
import at.spengergasse.hbgm.data.Patient;
import at.spengergasse.hbgm.data.Series;
import at.spengergasse.hbgm.data.Study;
import at.spengergasse.hbgm.ui.AbstractPatientBrowser;
import at.spengergasse.hbgm.ui.Viewer;
import at.spengergasse.hbgm.utils.IBaseComponent;
import at.spengergasse.hbgm.utils.IObserver;
//import com.sun.xml.internal.ws.api.streaming.XMLStreamWriterFactory;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.io.DicomInputStream;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.text.View;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class PatientBrowserImpl extends AbstractPatientBrowser {
    private DefaultMutableTreeNode root = new DefaultMutableTreeNode("Patienten");
    private DefaultTreeModel model = new DefaultTreeModel(root);
    private Viewer v;
    private ArrayList<IObserver> observers = new ArrayList<IObserver>();
    private Image image;
    private JScrollPane qPane;

    public PatientBrowserImpl(){
    this.setBorder(new TitledBorder("PatientBrowser"));
    this.setSize(50,60);
    }

    public Patient selectedPatient() {
    TreePath path = this.getSelectionPath();
    DefaultMutableTreeNode node = (DefaultMutableTreeNode)path.getLastPathComponent();
    Object userob = node.getUserObject();
    if(userob != null)
    {
         if(node.getUserObject() instanceof Image){
                 node = (DefaultMutableTreeNode)node.getParent();
             }
         if(node.getUserObject() instanceof Series){
            node = (DefaultMutableTreeNode)node.getParent();
             }
         if(node.getUserObject() instanceof Study){
            node = (DefaultMutableTreeNode)node.getParent();
             }
         if(node.getUserObject() instanceof Patient){
             return (Patient) node.getUserObject();
             }

         }
         return null;
     }

    public Study selectedStudy() {
         TreePath path = this.getSelectionPath();
         DefaultMutableTreeNode node = (DefaultMutableTreeNode)path.getLastPathComponent();
         Object userob = node.getUserObject();
         if(userob != null)
         {
         if(node.getUserObject() instanceof Image){
             node = (DefaultMutableTreeNode)node.getParent();
        }
         if(node.getUserObject() instanceof Series){
             node = (DefaultMutableTreeNode)node.getParent();
         }
         if(node.getUserObject() instanceof Study){
         return (Study) node.getUserObject();
         }

         }
        return null;
     }


    public Series selectedSeries() {
     TreePath path = this.getSelectionPath();
     DefaultMutableTreeNode node = (DefaultMutableTreeNode)path.getLastPathComponent();
    Object userob = node.getUserObject();
     if(userob != null)
     {
     if(node.getUserObject() instanceof Image){
     node = (DefaultMutableTreeNode)node.getParent();
     }
     if(node.getUserObject() instanceof Series){
     return (Series) node.getUserObject();
     }

     }return null;
     }

 public Image selectedImage() {
     TreePath path = this.getSelectionPath();
     if (path == null)
         return null;
     DefaultMutableTreeNode node = (DefaultMutableTreeNode)path.getLastPathComponent();
     Object userob = node.getUserObject();
     if(userob != null)
     {
     if(node.getUserObject() instanceof Image){
     return (Image) node.getUserObject();
     }

         }return null;
     }

     public void registerObserver(IObserver observer) {
     observers.add(observer);
     }

     public void removeObserver(IObserver observer) {
     observers.remove(observer);
    }

     public void configure(Viewer viewer) {
    this.v = viewer;
     v.getPatientRepository().registerObserver(this);
        }


    private void tree() {
        for (Patient p : v.getPatientRepository().patientList()) {
            DefaultMutableTreeNode pNode = new DefaultMutableTreeNode(p);
            root.add(pNode);
            for (Study st : p.getStudyList()) {
                DefaultMutableTreeNode stNode = new DefaultMutableTreeNode(st);
                pNode.add(stNode);
                for (Series se : st.getSeriesList()) {
                    DefaultMutableTreeNode seNode = new DefaultMutableTreeNode(se);
                    stNode.add(seNode);
                    for (Image i : se.getImageList()) {
                        DefaultMutableTreeNode iNode = new DefaultMutableTreeNode(i);
                        seNode.add(iNode);
                    }
                }
            }
        }
        this.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        this.addTreeSelectionListener(new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent e) {
                for (IObserver o : observers) {
                    o.changed(me());
                }
            }
        });
        setModel(model);
    }


     public void changed(IBaseComponent c) {
     this.tree();
     }

     private PatientBrowserImpl me(){
     return this;
     }

     public Image getImage() {
     return image;
     }

     public Image setImage() {
     return null;
     }

     public void setImage(Image image) {
     this.image = image;
    }
}