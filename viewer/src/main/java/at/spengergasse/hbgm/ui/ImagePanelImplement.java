package at.spengergasse.hbgm.ui;

import at.spengergasse.hbgm.data.Image;
import at.spengergasse.hbgm.image.IPixelMapper;
import at.spengergasse.hbgm.utils.IBaseComponent;
import at.spengergasse.hbgm.utils.IObserver;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class ImagePanelImplement extends AbstractImagePanel {
    private ArrayList<IObserver> observer = new ArrayList<>();
    private Image imag;
    Viewer viewer;
    private Image imagen;
    private IPixelMapper pm;
    private BufferedImage image;


    public ImagePanelImplement() throws Exception{

        repaint();

    }

    @Override
        public void paintComponent(Graphics g)
        {
            System.out.println("paintComponent");
            try
            {
                this.imag = viewer.getPatientBrowser().selectedImage();
                image = viewer.getPixelMapper().mapImage(imag);
                System.out.println("Imagepanel: image = " + imag);

            }catch (Exception e )
            {
                e.printStackTrace();
            }

            super.paintComponent(g);
            if(image != null)
                g.drawImage(image,0, 0, this);
        }

    
    public void setImage(BufferedImage image)
    {
        this.image =image;
        repaint();
    }

    public void registerObserver(IObserver observer) {
        this.observer.add(observer);
    }

    public void removeObserver(IObserver observer) {
        this.observer.remove(observer);
    }

    public void configure(Viewer viewer) {
        this.viewer = viewer;
        viewer.getPatientBrowser().registerObserver(this);
        viewer.getLookupTable().registerObserver(this);
        viewer.getControlPanel().registerObserver(this);

    }


    public void changed(IBaseComponent c) {

        this.repaint();
    }
}
