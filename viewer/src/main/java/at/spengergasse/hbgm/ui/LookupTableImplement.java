package at.spengergasse.hbgm.ui;

import at.spengergasse.hbgm.image.ILookupTable;
import at.spengergasse.hbgm.utils.IBaseComponent;
import at.spengergasse.hbgm.utils.IObserver;

import java.util.*;

/**
 * Created by Clarissa Martic on 24.11.2017.
 * 28.11.2017 Changed
 *
 * Letzte Änderung: 16.01.2018
 * RGB Berechnung optimiert
 */
public class LookupTableImplement implements ILookupTable {

    private ArrayList<IObserver> LT_observerList = new ArrayList<IObserver>();
    private Viewer viewer = new Viewer();
    private int center;
    private int alpha;
    private int width;

    public LookupTableImplement()
    {
        width = 255/2;
        center = 255/2;
        alpha = 100/2;

    }

    @Override
    public void registerObserver(IObserver observer) {
        LT_observerList.add(observer);
    }

    @Override
    public void removeObserver(IObserver observer) {
        for(int zaehler  = 0; zaehler< LT_observerList.size(); zaehler++){
            if(observer == LT_observerList.get(zaehler)){
                removeObserver(observer); // Observer löschen --> eigene Funktion
                //Übernommen aus ControlPanelImplement bzw. a
            }
        }
    }

    @Override
    public void configure(Viewer viewer) {
        this.viewer = viewer;
    }

    @Override
    public void changed(IBaseComponent c) {

    }

    @Override
    public int lookup(int value) {
        int newValue= (value - center - width/2)*255/width;
        if(newValue > 255)
        {
            newValue = 255;
        }
        if(newValue < 0){
            newValue =0;
        }
        int rgb = toRGB(newValue);
        return rgb;
    }

    @Override
    public void setCenter(int center) throws Exception {
        this.center = center;
    }

    @Override
    public int getCenter() {
        return 0;
    }

    @Override
    public void setWidth(int width) throws Exception {
        if(width !=0){
        this.width = width;}
        else if(width <= 0){

            throw new Exception("ERROR! Width cannot be <0! Please change!");
        }
    }

    @Override
    public int getWidth() {
        return 0;
    }

    @Override
    public void setAlpha(int alpha) throws Exception {
        this.alpha = alpha;
    }

    @Override
    public int getAlpha() {
        return 0;
    }

    private int toRGB(int value){
            return value << 16 | value << 8 | value | (this.alpha * 255 / 100) << 24;
    }
    private void NotifyObservers (){
        for(IObserver o : LT_observerList) //Benachrichtigung an alle Observer --> Änderung findet in DIESER Komponente statt
        {
            o.changed(this); // this --> diese Komponente
        }

    }

}
